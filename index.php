<?php

$action = '123';

if ($action == 123){
    echo 'int == Перевірка пройшла<br>';
}

if ($action === 123){
    echo 'int === Перевірка пройшла<br>';
} else echo 'int === Перевірка не пройшла<br>';

if ($action == '123'){
    echo 'string == Перевірка пройшла<br>';
}

if($action === '123'){
    echo 'string === Перевірка пройшла<br>';
}

var_dump(is_bool($action === 123));
var_dump(boolval($action));
var_dump((bool)$action);
var_dump((boolean)$action);